suppressPackageStartupMessages(library(HTSanalyzeR))
n_genes=100
geneList=numeric(n_genes)
names(geneList)=sprintf('gene%03d', seq_len(n_genes))
geneSet=names(geneList)[seq_len(10)]
geneList
geneSet
gseaScores(geneList, geneSet)

#!/usr/bin/env bash
# vim: set noexpandtab tabstop=2:

set -v
namedir=AML001_CD34_24H_X1_F1B10
namefile=AML001_CD34_24H_X1_F1B10.csv.tar.gz
outdir=$(mktemp -d)

infile="s3://data.lincscloud.org/l1000/level1/${namedir}/${namefile}"
outfile="$outdir/$namefile"

s3cmd get "$infile" "$outfile"

echo $outdir

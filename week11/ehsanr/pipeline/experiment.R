suppressPackageStartupMessages(library(rhdf5))
file = '/mnt/glusterfs_distributed/ehsanr/Documents/classproject/zspc_n1328098x22268.gctx'
firstDrug = h5read(file, index=list(NULL,1), name = "/0/DATA/0/matrix")
head(firstDrug)
Probes = h5read(file, name='0/META/ROW/id')

Probes = gsub(" ", "", Probes, fixed = TRUE)
rownames(firstDrug) = Probes

suppressPackageStartupMessages(library(hgu133plus2.db))
symbol_map=links(
  eval(
    parse(
      text='hgu133plus2SYMBOL'
      )
    )
  )
rownames(symbol_map)=symbol_map$probe_id
common_probes = intersect(Probes, symbol_map$probe_id)
firstDrug = firstDrug[common_probes, ]
names(firstDrug) = symbol_map[names(firstDrug), ]$symbol
head(firstDrug)

l1000gene = readRDS('../../../week10/ehsanr/l1000Genes.rds')
head(names(firstDrug))
head(l1000gene$GeneSymbol)
firstDrug = firstDrug[intersect(l1000gene$GeneSymbol, names(firstDrug))]
length(firstDrug)
saveRDS(firstDrug, 'experiment.rds')

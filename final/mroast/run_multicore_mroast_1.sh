#!/usr/bin/env bash
# vim: set noexpandtab tabstop=2:

lincslevel4='/mnt/glusterfs_distributed/ragse-f2015/lincs_l4_data/zspc_n1328098x22268.gctx'
file_landmarkGenes='/mnt/glusterfs_distributed/lijin/ragse/Git/drugrepositioning/week11/lijin/Landmark_Genes_n978.xlsx'
datadir='/mnt/glusterfs_distributed/lijin/ragse/Git/drugrepositioning/testdata_disease/1_crohn_disease/GSE6731'
file_metadata="/mnt/glusterfs_distributed/lijin/ragse/Git/drugrepositioning/testdata_disease/1_crohn_disease/metadata.txt"
name_anno_lib_disease='hgu95av2.db'
outRDS='result_disease_1_core.rds'
logs='run_logs_1.txt'

nohup time Rscript multicore_mroast.R -g "$file_landmarkGenes" -d "$datadir" -m "$file_metadata" -n "$name_anno_lib_disease" -L "$lincslevel4" -o "$outRDS" &> "$logs" &
wait $!


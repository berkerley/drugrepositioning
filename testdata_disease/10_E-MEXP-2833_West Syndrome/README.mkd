# West Syndrome - E-MEXP-2833 

## Download disease samples package from following link.

[http://www.ebi.ac.uk/arrayexpress/experiments/E-MEXP-2833/](http://www.ebi.ac.uk/arrayexpress/experiments/E-MEXP-2833/)

## The platform information.

~~~ 
Affymetrix GeneChip Human Genome U133 Plus 2.0 [HG-U133_Plus_2]
~~~

Please find the right annotation library.

# Drug source
No treatment exists right now.
https://rarediseases.org/rare-diseases/cdkl5/

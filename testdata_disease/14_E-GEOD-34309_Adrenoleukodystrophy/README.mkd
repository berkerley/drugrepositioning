# Adrenoleukodystrophy -  E-GEOD-34309

## Download disease samples package from following link.

[http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE34309](http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE34309)

## The platform information.

A-AFFY-37 - Affymetrix GeneChip Human Genome U133A 2.0 [HG-U133A_2]
~~~
source("https://bioconductor.org/biocLite.R")
biocLite("pd.hg.u133a.2")
~~~

Please find the right annotation library.

## drug source
[http://www.webmd.com/drugs/condition-4852-Adrenoleukodystrophy.aspx?diseaseid=4852&diseasename=Adrenoleukodystrophy&source=0](http://www.webmd.com/drugs/condition-4852-Adrenoleukodystrophy.aspx?diseaseid=4852&diseasename=Adrenoleukodystrophy&source=0)


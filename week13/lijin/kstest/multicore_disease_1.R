file_landmarkGenes <- "/mnt/glusterfs_distributed/lijin/ragse/Git/drugrepositioning/week11/lijin/Landmark_Genes_n978.xlsx"
datadir <- "/mnt/glusterfs_distributed/lijin/ragse/Git/drugrepositioning/testdata_disease/1_crohn_disease/GSE6731"
file_metadata <- "/mnt/glusterfs_distributed/lijin/ragse/Git/drugrepositioning/testdata_disease/1_crohn_disease/metadata.txt"
name_anno_lib_disease <- "hgu95av2.db"
lincslevel4 <- '/mnt/glusterfs_distributed/ragse-f2015/lincs_l4_data/zspc_n1328098x22268.gctx'

suppressPackageStartupMessages(library(affy))
suppressPackageStartupMessages(library(limma))
suppressPackageStartupMessages(library(name_anno_lib_disease, character.only=TRUE))
suppressPackageStartupMessages(library(hgu133plus2.db))
suppressPackageStartupMessages(library(annotate))
suppressPackageStartupMessages(library(xlsx))
suppressPackageStartupMessages(library(parallel))

landmarkGenes <- read.xlsx(file=file_landmarkGenes, sheetIndex=1)[,'Gene.Symbol']
metadata <- read.table(file_metadata, header=T)
sampleNames <- metadata[,"Sample"]
filenames <- metadata[,"Filename"]
ab=ReadAffy(filenames=filenames, celfile.path=datadir, sampleNames=sampleNames)
eset=rma(ab, verbose=F)
DiseaseID=featureNames(eset)
DiseaseSymbol=getSYMBOL(DiseaseID, name_anno_lib_disease)
GenesDisease <- intersect(DiseaseSymbol, landmarkGenes) # disease genes

suppressPackageStartupMessages(library(rhdf5)) # drug
DrugID = gsub(' ', '', h5read(lincslevel4, name='0/META/ROW/id'))
DrugSymbol=getSYMBOL(as.vector(DrugID), 'hgu133plus2.db')
GenesDrug <- intersect(DrugSymbol, landmarkGenes) # drug genes

commonGenes <- intersect(GenesDisease, GenesDrug)

matchGeneDisease <- DiseaseSymbol[!is.na(match(DiseaseSymbol, commonGenes))] # disease query
IndexDisease <- names(matchGeneDisease)
exprsDisease <- do.call(rbind, by(exprs(eset)[IndexDisease,], matchGeneDisease, colMeans))[commonGenes,]
str(exprsDisease)

suppressPackageStartupMessages(library(samr))
sampleType <- metadata[,'Type']
resDisease<-SAM(x=exprsDisease, y=sampleType, resp.type='Two class unpaired', genenames=commonGenes, geneid=commonGenes)
queryDisease <- rep(0, length(commonGenes))
names(queryDisease) <- commonGenes
if (resDisease$siggenes.table$ngenes.up > 0) {
  queryDisease[resDisease$siggenes.table$genes.up[,'Gene Name']] <- 1
}
if (resDisease$siggenes.table$ngenes.lo > 0) {
  queryDisease[resDisease$siggenes.table$genes.lo[,'Gene Name']] <- -1
}

matchGeneDrug <- DrugSymbol[!is.na(match(DrugSymbol, commonGenes))] # drug zscore
IndexDrug <- names(matchGeneDrug)

drugNames <- gsub(' ', '', h5read(lincslevel4, name='0/META/COL/id'))
matrix_run <- matrix(seq_along(drugNames), nrow=86) # 15443 * 86 = 1328098

source("/mnt/glusterfs_distributed/lijin/ragse/Git/drugrepositioning/week11/lijin/my_connectivity_score.R")
result <- do.call(c, mclapply(seq_len(ncol(matrix_run)), function(indexrun) {
  indexVector <- matrix_run[,indexrun]
  cat(indexVector[1], '\n')

  drugZscores <- h5read(lincslevel4, index=list(NULL, indexVector), name = "/0/DATA/0/matrix")
  H5close()
  rownames(drugZscores) <- DrugID
  colnames(drugZscores) <- drugNames[indexVector]

  connectivities <- sapply(seq_len(ncol(drugZscores)), function(indexCol) {
    drugZscore <- drugZscores[,indexCol]
    experimentDrug <- unlist(tapply(drugZscore[IndexDrug], matchGeneDrug, mean))[commonGenes]
    score <-  my_connectivity_score(experimentDrug, queryDisease)
    names(score) <- colnames(drugZscores)[indexCol]
    score
  })
  rm(drugZscores); gc()
  connectivities
}, mc.cores=25))


saveRDS(data.frame(result), file="result_disease_1_core.rds")


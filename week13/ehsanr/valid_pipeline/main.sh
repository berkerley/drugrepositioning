#!/usr/bin/env bash
# vim: set noexpandtab tabstop=2:

foldnumber=$1

R --no-save <<EOF
drug_number=$foldnumber
source('main.R')
EOF

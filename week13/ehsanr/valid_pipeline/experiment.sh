#!/usr/bin/env bash
# vim: set noexpandtab tabstop=2:

foldnumber=$1

R --no-save <<EOF
drug_number=$foldnumber
source('experiment.R')
EOF

---
title: "read.gctx"
author: "Benke Qu"
date: "November 24, 2015"
output: pdf_document
---

# read.gctx

This R script is used to parser gctx file named _zspc\_n1328098x22268.gctx_ located at "/mnt/glusterfs_distributed/ragse-f2015/lincs_l4_data" and prepare a GCTX object which has the following indespensible slots for future access.

mat: typeof matrix, which stores all the lincs level 4 drug information.

rid: typeof vector, 1:22268

cid: typeof vector, 1:1328098

rnames: typeof data.frame, all the probset names of length 22268. Its structure is like "221311\_x\_at"

cnames: typeof data.frame, all the drug names of length 1328098. Its structure is like "CPC001\_HA1E\_6H\_X2\_B3\_ DUO52HI53LO:D22"

The extracted drug signature is stored in the slot __mat__. And the above slots are all used to describe the extracted drug.

#Reference
[1] https://www.bioconductor.org/packages/release/bioc/html/rhdf5.html




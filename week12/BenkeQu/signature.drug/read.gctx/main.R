library(rhdf5)
source("read.gctx.R")
gctx_path <- "/mnt/glusterfs_distributed/ragse-f2015/lincs_l4_data/zspc_n1328098x22268.gctx"
# The next read.gctx reads in the the gctx file and get the first column as the first drug vector
gctx <- read.gctx(gctx_path, cid = 1)

suppressPackageStartupMessages(library(samr))
suppressPackageStartupMessages(library(affydata))
suppressPackageStartupMessages(library(affy))
suppressPackageStartupMessages(library(hgu133plus2.db))
suppressPackageStartupMessages(library(hgu95av2.db))
suppressPackageStartupMessages(library(rhdf5))


source("my_connectivity_score.R")

## read affy

##celfile.path=system.file("disease_GSE49566/disease_data", package="affydata")  

meta_data = read.table("metadata.txt",header=T)

disease_data = invisible(ReadAffy(filenames=as.vector(as.matrix(meta_data[3])), celfile.path="/mnt/glusterfs_distributed/ragse-f2015/disease_dataset/GSE49566") )
## rma

expression_matrix = invisible(exprs(affy::rma(disease_data)))
str(expression_matrix)   
print(nrow(expression_matrix))
## probeset to gene
source("probesTOgenes_hgu95.R")
gene_names = invisible(probesTOgenes(rownames(expression_matrix)))

print(gene_names)
length(gene_names)
print("unique")
length(unique(gene_names))
## samr
## larger than number of genes in human body
## will be intersected with the genes from the drug sample
n=25000
res_sam =invisible(SAM(
    expression_matrix
  , as.factor(as.vector(as.matrix(meta_data[2])))
  , resp.type='Two class unpaired'
  #, genenames = gene_names
  ))

#print("RES_SAM")
#head(res_sam)

#Convert the up and down regulatd genes into one induced vector from the samr results

if(!is.null(res_sam$siggenes.table$genes.lo) && !is.null(nrow(res_sam$siggenes.table$genes.lo)))
{
    genesLo = strtoi(res_sam$siggenes.table$genes.lo[,2])
} else {
    genesLo = strtoi(res_sam$siggenes.table$genes.lo[,2])
}


if(!is.null(res_sam$siggenes.table$genes.up) && !is.null(nrow(res_sam$siggenes.table$genes.up))) {
    genesUp = res_sam$siggenes.table$genes.up[,2]
} else {
    genesUp = res_sam$siggenes.table$genes.up[2]
}


query = c(rep(0,length(gene_names)))
names(query) = gene_names

if(length(genesLo) > 0){

    query[genesLo] = -1
}

if(length(genesUp) > 0){

    query[genesUp] = 1
}

## intersect query with L1000 genes
#drug_genes = as.vector(as.matrix(read.table("L1000GeneNames.txt")))
#query_intersect = query[drug_genes]

## drug data

file = '/mnt/glusterfs_distributed/ragse-f2015/lincs_l4_data/zspc_n1328098x22268.gctx'
Probes = h5read(file, name='0/META/ROW/id')
Drugs = h5read(file, name='0/META/COL')
n_Drugs = length(Drugs$id)
firstDrug = h5read(file, index=list(NULL,1), name = "/0/DATA/0/matrix")
#str(firstDrug)
#str(Probes)

Probes = gsub(" ", "", Probes, fixed = TRUE)
#str(Probes)
rownames(firstDrug) = Probes

## convert drug probeset to gene names
source("probesTOgenes_hgu133.R")

drug_gene_names = probesTOgenes(rownames(firstDrug))


## unique_index = unique(match(unique(gene_names),gene_names))
## queryUnique = query[unique_index]

common_index = intersect(drug_gene_names,gene_names)

firstDrugV = firstDrug[,1]

names(firstDrugV) = drug_gene_names


finalDrug = firstDrugV[common_index]
finalQuery = query[common_index]

print("finalDrug")
str(finalDrug)
print("finalQuery")
str(finalQuery)
## #print(finalQuery)
## str(which(finalQuery == -1))
## str(sort(which(unlist(query) == -1)))
## str(which(queryUnique == -1))
score = my_connectivity_score(finalDrug, finalQuery)

print(score)

#Disease signature
suppressPackageStartupMessages(library(affy))
suppressPackageStartupMessages(library(hgu95av2.db))
suppressPackageStartupMessages(library(samr))
suppressPackageStartupMessages(library(Biobase))
celfile.path = "/mnt/glusterfs_distributed/ragse-f2015/disease_dataset/GSE49566"
disease_data = ReadAffy(celfile.path = celfile.path)
norm_disease = rma(disease_data)
expr_disease = affy::exprs(norm_disease)
str(expr_disease)
source("probesTOgenes.R")
gene_samp = probesTOgenes(expr_disease)
res = SAM(expr_disease
          , as.factor(as.numeric(c(rep(2, 3), rep(1, 6))))
          , resp.type = 'Two class unpaired')
genesLo = res$siggenes.table$genes.lo[,"Gene Name"]
genesUp = res$siggenes.table$genes.up[,"Gene Name"]
inducedGenes = c(rep(0, length(gene_samp)))
names(inducedGenes) = rownames(gene_samp)
inducedGenes[as.numeric(genesLo)] = -1
inducedGenes[as.numeric(genesUp)] = 1

#str(inducedGenes)

#drug signature

suppressPackageStartupMessages(library(rhdf5))
suppressPackageStartupMessages(library(hgu133plus2.db))
source("probesTOgenes_hgu133.R")
file = '/mnt/glusterfs_distributed/ragse-f2015/lincs_l4_data/zspc_n1328098x22268.gctx'
#H5close()
firstDrug = h5read(file, index=list(NULL, 1), name = "/0/DATA/0/matrix")
Probes = gsub(" ", "", h5read(file, name='0/META/ROW/id'), fixed = TRUE)
rownames(firstDrug) = Probes

symbol_map=links(
  eval(
    parse(
      text='hgu133plus2SYMBOL'
    )
  )
)
rownames(symbol_map)=symbol_map$probe_id
common_probes = intersect(Probes, symbol_map$probe_id)
firstDrug = firstDrug[common_probes, ]
names(firstDrug) = symbol_map[names(firstDrug), ]$symbol

l1000gene = readRDS('/mnt/glusterfs_distributed/ragse-f2015/projectAssignment/week9/ywang654/l1000Genes.rds')
firstDrug = firstDrug[intersect(l1000gene$GeneSymbol, names(firstDrug))]

inducedGenes = inducedGenes[match(names(firstDrug), names(inducedGenes))]
inducedGenes = na.omit(inducedGenes)
#firstDrug
inducedGenes
#str(firstDrug)
firstDrug=unname(firstDrug)
inducedGenes=unname(inducedGenes)
#str(firstDrug)

#Calculate connectivity score

source('my_connectivity_score.R')
First_score = my_connectivity_score(firstDrug, inducedGenes)
First_score


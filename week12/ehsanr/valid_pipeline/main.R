#source('/mnt/glusterfs_distributed/ragse-f2015/projectAssignment/week9/ehsanr/connectivity_score/my_connectivity_score_2.R')
#source('/mnt/glusterfs_distributed/ragse-f2015/projectAssignment/week9/ehsanr/ks.test/my_ks.test_2.R')
source('/mnt/glusterfs_distributed/ragse-f2015/projectAssignment/week9/ehsanr/my_connectivity_score.R')

experiment = readRDS('experiment.rds')
query = readRDS('query.rds')
query = query[match(rownames(experiment), names(query))]
res=sapply(seq_len(ncol(experiment)), function(i) {
  my_connectivity_score(experiment[, i] ,query)
})

saveRDS(res, 'scores.rds')
